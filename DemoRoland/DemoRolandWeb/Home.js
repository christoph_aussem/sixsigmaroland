﻿(function () {
    "use strict";

    var cellToHighlight;
    var messageBanner;
    var chart;
    // Die Initialisierungsfunktion muss bei jedem Laden einer neuen Seite ausgeführt werden.
    Office.initialize = function (reason) {
        $(document).ready(function () {
            // Benachrichtigungsmechanismus initialisieren und ausblenden
            var element = document.querySelector('.MessageBanner');
            messageBanner = new components.MessageBanner(element);
            messageBanner.hideBanner();

            // Wenn nicht Excel 2016 verwendet wird, Fallbacklogik verwenden.
            if (!Office.context.requirements.isSetSupported('ExcelApi', '1.1')) {
                $("#template-description").text("Six Sigma Demo");
                $('#button-text').text("Anzeigen");
                $('#button-desc').text("Zeigt die Auswahl an.");

                //$('#highlight-button').click(displaySelectedCells);
                return;
            }

            $("#template-description").text("Six Sigma Demo");
            $('#button-text').text("Starten");
            //$('#button-desc').text("Hebt die größte Zahl hervor.");

            createUi();

            // Fügt einen Klickereignishandler für die Hervorhebungsschaltfläche hinzu.
            //$('#highlight-button').click(hightlightHighestValue);
            $('#highlight-button').click(berechnen);
        });
    };


    /*
     * Erstellt das Excel Sheet mit allein Eingabebereichen und dem Diagramm
     */
    function createUi() {
        var values = [
            [100, Math.floor(Math.random() * 1000), Math.floor(Math.random() * 1000)],
            [Math.floor(Math.random() * 1000), Math.floor(Math.random() * 1000), Math.floor(Math.random() * 1000)],
            [Math.floor(Math.random() * 1000), Math.floor(Math.random() * 1000), Math.floor(Math.random() * 1000)]
        ];

        // Führt einen Batchvorgang für das Excel-Objektmodell aus.
        Excel.run(function (ctx) {
            // Erstellt ein Proxyobjekt für die aktive Blattvariable
            var sheet = ctx.workbook.worksheets.getActiveWorksheet();
            // Reiht einen Befehl zum Schreiben der Beispieldaten in das Arbeitsblatt in die Warteschlange ein.
            //sheet.getRange("B3:D5").values = values;

            var coords = [
                { "a": "B4:B25", b: "C5:C25", c: "C4" },
                { "a": "E4:E25", b: "F5:F25", c: "F4" },
                { "a": "H4:H25", b: "I5:I25", c: "I4" },
                { "a": "K4:K25", b: "L5:L25", c: "L4" },
            ];

            // i und xi spalten formatieren
            for (var j = 0; j < coords.length; j++) {
                var coord = coords[j];

                var values = [];
                for (var i = 0; i < 22; i++) values.push([i]);
                values[0] = ["i"];
                var myrange = sheet.getRange(coord.a);
                myrange.values = values;
                myrange.format.HorizontalAlignment = Excel.HorizontalAlignment.center;
                myrange.format.columnWidth = 20;
                // Formatierung der xi-Zellen
                myrange = sheet.getRange(coord.b);
                myrange.format.fill.color = "#DCE6F1";
                myrange.format.font.bold = true;
                myrange.format.columnWidth = 40;
                //dobordes(myrange);
                dobordes(sheet.getRange(coord.a));
                dobordes(sheet.getRange(coord.b));

                sheet.getRange(coord.c).values = [["xi"]];
                dobordes(sheet.getRange(coord.c));


            }

            var xis = sheet.getRange("C5:C25");
            var xisamplevalues = [];
            for (var i = 0; i < 21; i++) xisamplevalues.push([i]);
            xis.values = xisamplevalues;

            {
                var myrange = sheet.getRange("N7:N14");
                myrange.values = [["Artikel:"],
                ["A-Nummer"],
                ["Zeichnung:"],
                ["Prüfer:"],
                ["Messmittel:"],
                ["Datum:"],
                ["Merkmal:"],
                ["Maßeinheit:"]];

                myrange = sheet.getRange("O7:P14");
                myrange.format.fill.color = "#DCE6F1";
                myrange.format.font.bold = true;
                myrange.merge(true);

                myrange = sheet.getRange("O7:P14");
                myrange.format.fill.color = "#DCE6F1";
                dobordes(sheet.getRange("N7:P14"));

                myrange = sheet.getRange("N4:P4");
                myrange.format.fill.color = "#DCE6F1";
                myrange.values = [["Werte können geändert werden:", "", ""]];
                myrange.merge(true);
                dobordes(myrange);

                myrange = sheet.getRange("N5:P5");
                myrange.format.fill.color = "#c4d79b";
                myrange.values = [["Werte werden berechnet (Blatt Berechnung)", "", ""]];
                myrange.merge(true);
                dobordes(myrange);

                myrange = sheet.getRange("N2");
                myrange.format.font.size = 22;
                myrange.values = [["Prozess- und Maschinenfähigkeit"]];
                myrange = sheet.getRange("N2:T2");
                myrange.merge(true);
                dobordes(myrange);
            }





            sheet.getRange("A1").values = "All good5!";
            sheet.getRange("A1").format.HorizontalAlignment = Excel.HorizontalAlignment.center;
            sheet.getRange("A1").format.textOrientation = 0;
            sheet.getRange("A1").format.HorizontalAlignment = "Right";


            var berechnungsSheet = createBerechnungsSheet(ctx);
            drawChart(sheet, berechnungsSheet);


            //Spaltenbreiten einstellen
            sheet.getRange("A:Z").format.autofitColumns();
            sheet.getRange("D:D").format.columnWidth = 10;
            sheet.getRange("G:G").format.columnWidth = 10;
            sheet.getRange("J:J").format.columnWidth = 10;
            sheet.getRange("M:M").format.columnWidth = 10;
            for (var j = 0; j < coords.length; j++) {
                var coord = coords[j];
                var myrange = sheet.getRange(coord.a);
                myrange.format.columnWidth = 20;

                var myrange = sheet.getRange(coord.b);
                myrange.format.columnWidth = 40;

            }
            // Führt die in die Warteschlange eingereihten Befehle aus und gibt eine Zusage zum Angeben des Abschlusses der Aufgabe zurück.




            return ctx.sync();
        })
            .catch(errorHandler);
    }

    /*
     * Erstellt das Berechnungs Sheet und die benannten Bereiche darauf
     */
    function createBerechnungsSheet(ctx) {

        var berechnung = ctx.workbook.worksheets.add("Berechnung");

        var names = ctx.workbook.names;
        names.add("Mittelwert", berechnung.getRange("P13"));
        names.add("Standardabweichung", berechnung.getRange("P14"));

        berechnung.getRange("O13").values = [["Mittelwert"]];
        berechnung.getRange("O14").values = [["Standardabweichung"]];
        dobordes(berechnung.getRange("O13:P14"));

        return berechnung;
    }


    /*
     * Wird beim Click auf den Button ausgeführt
     */
    function berechnen() {
        Excel.run(function (ctx) {
            //https://docs.microsoft.com/de-de/javascript/api/excel/excel.chart?view=excel-js-preview

            // Erstellt ein Proxyobjekt für den ausgewählten Bereich und lädt seine Eigenschaften
            var sheet = ctx.workbook.worksheets.getActiveWorksheet();

            var xis = sheet.getRange("C5:C25");
            xis.load("address, values");

            chart = sheet.charts.getItem("Chart 1");
            chart.legend.visible = false;
            //chart.title.text = "foooo";

            //var series = chart.series;
            //var series0 = series.getItemAt(0);
            ////https://docs.microsoft.com/de-de/javascript/api/excel/excel.chartseries?view=excel-js-preview
            //series0.setValues(xis);


            var names = ctx.workbook.names;
            var range = names.getItem('Mittelwert').getRange();
            range.load('address');

            /*
            * Beispiel zur Verwendung der Excel Funktionen über Javascript
            */
            //https://docs.microsoft.com/en-us/office/dev/add-ins/excel/excel-add-ins-worksheet-functions
            var mittelwert = ctx.workbook.functions.average(xis);
            mittelwert.load('value');

            var standardabweichung = ctx.workbook.functions.stDevA(xis);
            standardabweichung.load('value');

            ctx.sync()
                .then(function () {

                    var berechnet1 = [];
                    for (var i = 0; i < xis.values.length; i++) {
                        berechnet1.push([xis.values[i][0]]);
                        //if (xis.values[i][1] === "")
                        //    break;
                        //else
                        //    berechnet1.push(xis.values[i][1]);
                    }



                    var berechnungssheet = ctx.workbook.worksheets.getItem("Berechnung");
                    berechnungssheet.getRange("P13:P14").values = [[mittelwert.value], [standardabweichung.value]];
                    var resultsrange = berechnungssheet.getRange("AF5:AF" + (berechnet1.length + 5 - 1));

                    resultsrange.values = berechnet1;

                    // -3 Sigma
                    resultsrange = berechnungssheet.getRange("AI5:AI" + (berechnet1.length + 5 - 1));
                    var minusdreisigma = [];
                    for (var j = 0; j < berechnet1.length; j++) {
                        minusdreisigma.push([mittelwert.value - 3 * standardabweichung.value]);
                    }
                    resultsrange.values = minusdreisigma;
                    ctx.sync();
                });
        });
    }

    /*
     * Zeichnet den Chart
     */
    function drawChart(eingabesheet, berechnungsheet) {
        var dataRange = berechnungsheet.getRange("AF5:AF25");

        chart = eingabesheet.charts.add("Line", dataRange, "auto");
        chart.setPosition("Q4", null);
        chart.width = 300;
        chart.title.text = "Stichprobe chronologisch";
        chart.axes.categoryAxis.visible = false;
        chart.legend.format.font.color = "white";
        //chart.legend.visible = false;
        //chart.legend.show = right;
        //chart.format.fill.setSolidColor("#36404A");
        //chart.title.format.font.color = "white";
        //chart.axes.valueAxis.format.font.color = "white";

        //var seriesCollection = sheet.charts.getItemAt(0);
        // var  rangeSelection = sheet.getRange("C2:C7");
        //let xRangeSelection = sheet.getRange("A1:A7");

        // Add a series.
        dataRange = berechnungsheet.getRange("AI5:AI25");
        var newSeries = chart.series.add("Qtr2");
        newSeries.setValues(dataRange);
        //newSeries.setXAxisValues(xRangeSelection);


    }

    /*
     * Zeichnet die Excel Borders
     */
    function dobordes(myrange) {
        myrange.format.borders.getItem('InsideHorizontal').style = 'Continuous';
        myrange.format.borders.getItem('InsideVertical').style = 'Continuous';
        myrange.format.borders.getItem('EdgeBottom').style = 'Continuous';
        myrange.format.borders.getItem('EdgeLeft').style = 'Continuous';
        myrange.format.borders.getItem('EdgeRight').style = 'Continuous';
        myrange.format.borders.getItem('EdgeTop').style = 'Continuous';
    }



    // Eine Hilfsfunktion zur Behandlung von Fehlern.
    function errorHandler(error) {
        // Stellen Sie immer sicher, dass kumulierte Fehler abgefangen werden, die bei der Ausführung von "Excel.run" auftreten.
        showNotification("Fehler", error);
        console.log("Error: " + error);
        if (error instanceof OfficeExtension.Error) {
            console.log("Debug info: " + JSON.stringify(error.debugInfo));
        }
    }

    // Eine Hilfsfunktion zum Anzeigen von Benachrichtigungen.
    function showNotification(header, content) {
        $("#notification-header").text(header);
        $("#notification-body").text(content);
        messageBanner.showBanner();
        //messageBanner.toggleExpansion();
    }
})();
